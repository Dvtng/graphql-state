import React, { Component } from "react";
import gql from "graphql-tag";
import connect from "../lib/connect";

const withSearchTerms = connect(
  gql`
    query {
      view {
        searchTerms
      }
    }
  `,
  ({ data }) => ({
    searchTerms: data.view.searchTerms
  })
);

const withSearchResults = connect(
  gql`
    query($searchTerms: String!) {
      search(type: REPOSITORY, query: $searchTerms, first: 10) {
        edges {
          node {
            ... on Repository {
              nameWithOwner
              description
            }
          }
        }
      }
      color
    }
  `,
  ({ data }) => ({
    searchResults: data.search.edges.map(edge => edge.node),
    color: data.color
  })
);

class SearchResults extends Component {
  render() {
    const { searchResults, searchTerms, color } = this.props;
    return (
      <div style={{ color }}>
        <p>
          Search results for: <i>{searchTerms}</i>
        </p>
        {searchResults.map(repo => (
          <div key={repo.nameWithOwner}>
            <b>{repo.nameWithOwner}</b>
            <p>{repo.description}</p>
          </div>
        ))}
      </div>
    );
  }
}

export default withSearchTerms(withSearchResults(SearchResults));
