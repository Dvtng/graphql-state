import React, { Component } from "react";
import gql from "graphql-tag";
import connect from "../lib/connect";

const withData = connect(
  gql`
    query {
      view {
        searchTerms
      }
    }
  `,
  ({ data }) => ({
    searchTerms: data.view.searchTerms
  })
);

const withOnChange = connect(
  gql`
    mutation($searchTerms: String!) {
      changeSearchTerms(searchTerms: $searchTerms) {
        searchTerms
      }
    }
  `,
  ({ mutate }) => ({
    onChange: searchTerms => mutate({ variables: { searchTerms } })
  })
);

class SearchInput extends Component {
  onChange = e => {
    this.props.onChange(e.target.value);
  };

  render() {
    const { searchTerms } = this.props;
    return <input value={searchTerms} onChange={this.onChange} />;
  }
}

export default withOnChange(withData(SearchInput));
