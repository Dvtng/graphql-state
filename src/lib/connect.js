// Wrapper around react-apollo's graphql for a more seamless API

import React from "react";
import { graphql } from "react-apollo";

export default (query, mapToProps) => WrappedComponent =>
  graphql(query)(
    ({ data, mutate, ...ownProps }) =>
      data && data.loading ? null : (
        <WrappedComponent {...ownProps} {...mapToProps({ data, mutate })} />
      )
  );
