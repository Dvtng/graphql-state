import { getColor } from "./rest";

const state = {
  searchTerms: "graphql"
};

const queries = {
  view: () => state,
  color: getColor
};

const mutations = {
  changeSearchTerms: ({ searchTerms }) => {
    state.searchTerms = searchTerms;
    return state;
  }
};

export default {
  ...queries,
  ...mutations
};
