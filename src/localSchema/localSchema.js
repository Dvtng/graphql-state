import { buildSchema } from "graphql";

export default buildSchema(`
  type View {
    searchTerms: String!
  }

  type Query {
    view: View!
    color: String!
  }

  type Mutation {
    changeSearchTerms(searchTerms: String!): View!
  }
`);
