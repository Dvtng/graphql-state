export const getColor = () =>
  fetch("http://www.colr.org/json/color/random")
    .then(response => response.json())
    .then(json => `#${json.colors[0].hex}`);
