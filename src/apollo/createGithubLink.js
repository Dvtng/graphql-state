import { HttpLink } from "apollo-link-http";
import { ApolloLink, concat } from "apollo-link";

export default () => {
  const token = localStorage.getItem("github.token") || null;
  const authLink = new ApolloLink((operation, forward) => {
    operation.setContext({
      headers: {
        authorization: `bearer ${token}`
      }
    });
    return forward(operation);
  });

  const httpLink = new HttpLink({ uri: "https://api.github.com/graphql" });

  return concat(authLink, httpLink);
};
