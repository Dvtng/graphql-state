import { ApolloLink, Observable } from "apollo-link";
import { execute } from "graphql";
import localSchema from "../localSchema/localSchema";
import localResolvers from "../localSchema/localResolvers";
import extractQueryOnRootSelections from "./extractQueryOnRootSelections";

const localSelections = new Set(Object.keys(localResolvers));

const isLocal = selection => localSelections.has(selection.name.value);

const not = fn => (...args) => !fn(...args);

// Apollo is particular about errors.
// It will throw if there's an error object at all, even if it's an empty array.
const mergeErrors = (a, b) =>
  a == null && b == null ? null : (a || []).concat(b || []);

export default () => {
  return new ApolloLink((operation, forward) => {
    const localQuery = extractQueryOnRootSelections(operation.query, isLocal);

    if (localQuery === null) {
      return forward(operation);
    }

    const remoteQuery = extractQueryOnRootSelections(
      operation.query,
      not(isLocal)
    );

    return new Observable(observer => {
      operation.query = remoteQuery;
      const remoteObservable = remoteQuery
        ? forward(operation)
        : Observable.of({ data: {} });
      const remoteSubscription = remoteObservable.subscribe({
        next: remoteResult => {
          execute(
            localSchema,
            localQuery,
            localResolvers,
            operation.getContext(),
            operation.variables,
            operation.name
          ).then(localResult => {
            const mergedResult = {
              data: {
                ...remoteResult.data,
                ...localResult.data
              },
              errors: mergeErrors(remoteResult.errors, localResult.errors)
            };
            observer.next(mergedResult);
            observer.complete();
          });
        },
        error: observer.error.bind(observer)
      });
      return () => {
        remoteSubscription.unsubscribe();
      };
    });
  });
};
