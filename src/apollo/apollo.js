import { ApolloClient } from "apollo-client";
import { InMemoryCache, defaultDataIdFromObject } from "apollo-cache-inmemory";
import createLocalLink from "./createLocalLink";
import createGithubLink from "./createGithubLink";

export const createClient = () =>
  new ApolloClient({
    link: createLocalLink().concat(createGithubLink()),
    cache: new InMemoryCache({
      dataIdFromObject: object => {
        if (object.__typename === "View") {
          return "View:singleton";
        }
        return defaultDataIdFromObject(object);
      }
    })
  });
