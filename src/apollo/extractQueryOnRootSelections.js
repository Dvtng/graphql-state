// Updates the value at the path on the object.
// If the path can't be reached, null will be returned.
// If the updated value will be null, null will be returned.
// Otherwise a copy of the object with the updated value will be returned.
const updateOrNull = (object, path, updater) => {
  if (path.length === 0) {
    return updater(object);
  }
  const childProperty = path[0];
  const child = object[childProperty];
  if (typeof child === "undefined") {
    return null;
  }
  const updatedChild = updateOrNull(child, path.slice(1), updater);
  return updatedChild === null
    ? null
    : {
        ...object,
        [childProperty]: updatedChild
      };
};

// Returns the array if it's not empty, or null if it's empty
const arrayOrNull = array => (array.length ? array : null);

// Extracts a query containing only root selections that match the selectionCondition.
// Returns null if the resulting query is effectively empty.
export default (query, selectionCondition) =>
  updateOrNull(query, ["definitions"], definitions =>
    arrayOrNull(
      definitions
        .map(definition =>
          updateOrNull(definition, ["selectionSet", "selections"], selections =>
            arrayOrNull(selections.filter(selectionCondition))
          )
        )
        .filter(item => item != null)
    )
  );
