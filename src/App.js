import React, { Component } from "react";
import { ApolloProvider } from "react-apollo";
import { createClient } from "./apollo/apollo";
import SearchInput from "./components/SearchInput";
import SearchResults from "./components/SearchResults";

export default class App extends Component {
  apolloClient = createClient();

  render() {
    return (
      <ApolloProvider client={this.apolloClient}>
        <div>
          <SearchInput />
          <SearchResults />
        </div>
      </ApolloProvider>
    );
  }
}
